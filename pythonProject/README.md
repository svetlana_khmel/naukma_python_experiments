 !!!!pip3 install numpy

#Value iteration algorithm to solve a Markov Decision Process (MDP)

- A set of states. This represents all the possible locations for the agent in the environment.
- A set of actions. This represents all the actions the agent can take at any given state.
- Transition probabilities. This represents the probability that the action the agent attempts will be successful (or not).
- Rewards, which are the values of arriving in a specific state.
- A discount factor γ which is meant to diminish the value of future rewards compared to instant rewards.


- The goal of the agent in a MDP is to find the optimal policy, which is the set of optimal actions to take at any given state.

- The value of being in a state is equal to the maximum of the immediate reward of that state (R) plus the discounted rewards of every adjacent state (St+1), considering the transition probabilities.

###Description
This code is an implementation of the value iteration algorithm to find the optimal policy of a Markov Decision Process (MDP).

environment.png is a visual representation of the environment used in the code.

mdp.py contains the code used to create the environment as well as the algorithm.

Go here for an article describing the code and the algorithm: https://towardsdatascience.com/how-to-code-the-value-iteration-algorithm-for-reinforcement-learning-8fb806e117d1

###Requirements
`
Python 3

Numpy
`


##Install python 3

- brew install pyenv
- pyenv install 3.7.3
- pyenv global 3.7.3

- pyenv version

#### Don't forget to update pip to pip3!
- pip -V
- which pip3

https://opensource.com/article/19/5/python-3-default-mac

2) pip3 install virtualenv
3) -virtualenv -p python3 envname
4) pip install --upgrade virtualenv
-virtualenv --python=/usr/bin/python3 ivalue_env
5) https://towardsdatascience.com/how-to-code-the-value-iteration-algorithm-for-reinforcement-learning-8fb806e117d1
6) https://towardsdatascience.com/how-to-code-the-value-iteration-algorithm-for-reinforcement-learning-8fb806e117d1