# coding=utf-8
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.



#import libraries
import numpy as np


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print("Hi, {0}".format(name))  # Press ⌘F8 to toggle the breakpoint.


#def


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')


    def is_attack(i, j, board, N):
        # checking for column j
        for k in range(1, i):
            if (board[k][j] == 1):
                return True

        # checking upper right diagonal
        k = i - 1
        l = j + 1
        while (k >= 1 and l <= N):
            if (board[k][l] == 1):
                return True
            k = k + 1
            l = l + 1

        # checking upper left diagonal
        k = i - 1
        l = j - 1
        while (k >= 1 and l >= 1):
            if (board[k][l] == 1):
                return True
            k = k - 1
            l = l - 1

        return False


    def n_queen(row, n, N, board):
        if (n == 0):
            return True

        for j in range(1, N + 1):
            if (not (is_attack(row, j, board, N))):
                board[row][j] = 1

                if (n_queen(row + 1, n - 1, N, board)):
                    return True

                board[row][j] = 0  # backtracking
        return False


    if __name__ == '__main__':
        #board = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]


        #n_queen(1, 4, 4, board)

        # printing the matix
        #for i in range(1, 5):
            #print(board[i][1:])
        N = 8


        def is_valid(i, j, sol):
            if (i >= 1 and i <= N and j >= 1 and j <= N):
                if (sol[i][j] == -1):
                    return True
            return False


        def knight_tour(sol, i, j, step_count, x_move, y_move):
            if (step_count == N * N):
                return True

            for k in range(0, 8):
                next_i = i + x_move[k]
                next_j = j + y_move[k]

                if (is_valid(next_i, next_j, sol)):
                    sol[next_i][next_j] = step_count
                    if (knight_tour(sol, next_i, next_j, step_count + 1, x_move, y_move)):
                        return True
                    sol[next_i][next_j] = -1;  # backtracking

            return False


        def start_knight_tour():
            sol = []

            for i in range(0, N + 1):
                a = [0] + ([-1] * N)
                sol.append(a)

            x_move = [2, 1, -1, -2, -2, -1, 1, 2]
            y_move = [1, 2, 2, 1, -1, -2, -2, -1]

            sol[1][1] = 0  # placing knight at cell(1, 1)

            if (knight_tour(sol, 1, 1, 1, x_move, y_move)):
                for i in range(1, N + 1):
                    print(sol[i][1:])
                return True
            return False


        if __name__ == '__main__':
            print(start_knight_tour())

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
