import numpy as np

number_of_stages = 3 # no of states
A = ['l', 'r', 'd', 'l']  # actions
number_of_actions = 4

# R [from state][action]
#  Reward for each --->step<---

# state1 state2 state3
# R = [[-5, -5, -1, -1],
#      [-100, -5, -5, -1],
#      [-100, -1, -5, 100],
#
#      [-5, -1, -100, -1],
#      [-100, -5, -5, -1],
#      [-1, -100, -1, 100],
#
#      [-1, -1, -1, -1],
#      [-100, -1, -1, -5],
#      [-100, -100, -1, 100],
# ]

R = [[5, 5, 1, 1],
     [-100, 5, 5, 1],
     [-100, 1, 5, 100],

     [-5, 1, 100, 1],
     [-100, 5, 5, 1],
     [-1, 100, 1, 100],

     [1, 1, 1, 1],
     [100, 1, 1, 5],
     [100, 100, 1, 100],
]
# P [from state] [to state] [action]

# Probability of actions
P = [
    [[0.5, 0.2, 0.1, 0.3], [0.1, 0.7, 0.1, 0.1], [0, 0, 0, 0], [0, 0, 0, 0]],
    [[0.5, 0.3, 0.1, 0.2], [0, 0, 0, 0], [0.2, 0.5, 0.1, 0.3], [0, 0, 0, 0]],
    [[0, 0, 0, 0], [0.4, 0.1, 0.1, 0.3], [0, 0, 0, 0], [0, 0.5, 0.5, 0]],
]

delta = 0.01
gamma = 0.26
max_diff = 0

V = [[0, 0, 0, 10], [-100, 0, 0, 10], [0, 0, 0, 10], [0, 0, 0, 100]]  # utilities of each state

print('Iteration',"|", '      0      ',"|", '       1      ',"|", '2      ',"|", '        3      ', 'Maximum difference')

for time in range(30):
    print('-1e9 is:', -1e9 )
    Vnew = [[-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1]]
    for i in range(3):
        for a in range(number_of_actions):
            cur_val = 0
            for j in range(number_of_stages):
                cur_val += P[i][j][a]*V[i][j]
            cur_val *= gamma
            cur_val += R[i][a]
            Vnew[i][j] = max(Vnew[i][j], cur_val)
    max_diff = 0
    for i in range(4):
        max_diff = max(max_diff, abs(V[i][j]-Vnew[i][j]))
    V = Vnew
    if(max_diff < delta):
        break

# one final iteration to determine the policy
Vnew = [[-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1]]
policy = [['NA', 'NA', 'NA', 'NA'],
          ['NA', 'NA', 'NA', 'NA'],
          ['NA', 'NA', 'NA', 'NA'],
          ['NA', 'NA', 'NA', 'NA']]
for i in range(3):
    for a in range(number_of_actions):
        cur_val = 0
        for j in range(number_of_stages):
            cur_val += P[i][j][a]*V[i][j]

        cur_val *= gamma
        cur_val += R[i][a]

        if(Vnew[i][a] < cur_val):
            policy[i][a] = A[a]
            Vnew[i][a] = max(Vnew[i][a], cur_val)
print('')
print('Final table Values:')
for i in range(4):
    print('row', Vnew[i])

maze = np.full((number_of_stages, number_of_actions), " ");

maze[2][3] = '$'
print('')
print(maze)

for i in range(3):
    max_index = Vnew[i].index(max(Vnew[i]));
    maze[i][max_index] = 'x'
print('')
print(maze)
