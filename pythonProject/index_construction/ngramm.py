ngramDict = {}
ngramDictConverted = {}

def splitWordsToGrams(word):
    for i in range(len(word)):
        if i == 0:
            ngram_list.append(word[0] + word[1])
        elif i == len(word) - 2:
            ngram_list.append(word[len(word) - 2] + word[len(word) - 1])
        elif i == len(word) - 1:
            continue
        else:
            ngram_list.append(word[i] + word[i + 1] + word[i + 2])


def convertNgramm(ngramDict):
    for key in ngramDict:
        for gram in ngramDict[key]:
            if not ngramDictConverted.get(gram):
                ngramDictConverted[gram] = []

            ngramDictConverted[gram].append(key)
            ngramDictConverted[gram] = list(set(ngramDictConverted[gram]))

    print('ngramDictConverted: ', ngramDictConverted)
    return ngramDictConverted


def createNgramm(text_array):
    for word in text_array:
        ngram_list = []
        word = word.lower()

        if len(word) > 3:
            for i in range(len(word)):
                if i == 0:
                    ngram_list.append("$" + word[0].lower()+word[1].lower())
                elif i == len(word) - 2:
                    ngram_list.append(word[len(word)-2].lower() + word[len(word) - 1].lower() + "$")
                elif i == len(word) - 1:
                    continue
                else:
                    ngram_list.append(word[i].lower() + word[i+1].lower() + word[i+2].lower())

                ngramDict[word] = ngram_list

    return convertNgramm(ngramDict)