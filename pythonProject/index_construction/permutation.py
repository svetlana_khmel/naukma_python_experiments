from collections import deque

permutationDict = {}

def createPermutation(text_array):

    for word in text_array:
        permutation_list = []
        original_word = word

        word += "$"
        for i in range(len(word)):

            symbols = deque(word)
            symbols.rotate(i)
            symbols = ''.join(map(str, list(symbols)))
            permutation_list.append(symbols)

            permutationDict[original_word] = list(permutation_list)

    return permutationDict
