import os
import re


import ngramm
import permutation
import write_to_file

inputDirectory = "input/"
list_of_files = []
ngramDict = {}
permutationDict = {}

def intersection(lst1, lst2):
    print('List 1:', lst1)
    print('List 2:', lst2)

    return list(set(lst1) & set(lst2))

def search(start_word, end_word, ngramDict):
    start_list = []
    end_list = []
    if start_word:
        start_list = ngramDict['$' + start_word]
    if end_word:
        end_list = ngramDict[end_word + '$']
    else:
        print('Not found')
    print("Found: ", intersection(start_list, end_list))

def searchStartJocker(start_word, end_word, ngramDict):
    start_list = []
    end_list = []
    if start_word:
        start_list = ngramDict[start_word]
    if end_word:
        end_list = ngramDict[end_word + '$']
    else:
        print('Not found')
    print("Found: ", intersection(start_list, end_list))
def searchEndJockert(start_word, end_word, ngramDict):
    start_list = []
    end_list = []
    if start_word:
        start_list = ngramDict['$'+start_word]
    if end_word:
        end_list = ngramDict[end_word]
    print("Found: ", intersection(start_list, end_list))

def searchMiddle(middle_word, ngramDict):
    start_list = []
    end_list = []

    print('middle_word:', middle_word)
    # if middle_word and  middle_word[middle_word]:
    if ngramDict.get(middle_word):
        print('Found:',  ngramDict[middle_word])
    if ngramDict.get('$' + middle_word):
        start_list = ngramDict['$' + middle_word]
        print('Found:',  ngramDict['$' + middle_word])
    if ngramDict.get(middle_word + '$' ):
        end_list = ngramDict[middle_word + '$']
        print('Found:',  ngramDict[middle_word +'$'])
    if len(start_list) != 0 and len(end_list) !=0:
        print("Found: ", intersection(start_list, end_list))

# Пошук з джокерами
def getQuery(ngramDict):
    word = input('Make a query: ')
    word = word.lower()

    if word.find('*') != -1:
        jockerPosition = word.index('*')
        if jockerPosition == 0:
           start_word = word[jockerPosition + 1: 4]
           end_word = word[len(word)-2: len(word)+1]
           searchStartJocker(start_word.lower(), end_word.lower(), ngramDict)
        elif jockerPosition == 1:
            start_word = word[jockerPosition + 1: 5]
            end_word = word[len(word) - 2: len(word) + 1]
            searchStartJocker(start_word.lower(), end_word.lower(), ngramDict)
        elif jockerPosition == len(word)-1:
            start_word = word[0: 2]
            end_word = word[len(word) - 4: len(word)-1]
            searchEndJockert(start_word.lower(), end_word.lower(), ngramDict)
        elif jockerPosition == len(word)-2:
            start_word = word[0: 2]
            end_word = word[len(word) - 5: len(word)-2]
            searchEndJockert(start_word.lower(), end_word.lower(), ngramDict)
        else:
            start_word = word[0: jockerPosition]
            end_word = word[jockerPosition+1: len(word)]

            if len(start_word) >= 3:
                print('start_word do ', start_word)
                start_word = start_word[0:2]
                print('start_word after ', start_word)

            if len(end_word) >= 3:
                print('end_word do ', end_word)
                print('len(end_word-1) do ', len(end_word))
                end_word = end_word[len(end_word)-2: len(end_word)]
                print('end_word after ', end_word)

            print('start_word ', start_word)
            print('end_word ', end_word)

            search(start_word.lower(), end_word.lower(), ngramDict)
    else:
        print('word ', word)
        searchMiddle(word.lower(), ngramDict)


def processFile(text, type):
    processedText = re.sub(r'[^\w\s]', '', text)

    if type == 'ngram':
        # Будуємо 3-грамний індекс для словника
        return ngramm.createNgramm(processedText.split())

    if type == 'permutation':
        # Побудувати перестановочний індекс для словника
        return permutation.createPermutation(processedText.split())

def readDirectory():
    for filename in os.listdir(inputDirectory):

        index = len(list_of_files)
        subarray = [index, filename]
        list_of_files.append(subarray)

        with open(os.path.join(inputDirectory, filename), 'r') as f:
            text = f.read()
            ngramDict = processFile(text, 'ngram')
            permutationDict = processFile(text, 'permutation')

    print(ngramDict)
    # print(permutationDict)

    write_to_file.writeToFile('Threegram_Index.txt', 'txt', ngramDict)
    write_to_file.writeToFile('Permutation_Index.txt', 'txt', permutationDict)
    getQuery(ngramDict)

readDirectory()



