# import os
# import re
# import sys
import json

def writeToFile(name, extention, content):
    if extention == 'txt':
        with open('output/'+ name, 'w') as f:
            f.write(str(content))
            # getFileSize(name)
    if extention == 'json':
        json_object = json.dumps(content, indent=4)
        with open('output/'+name, 'w') as f:
            json.dump(content, f)
            # getFileSize(name)