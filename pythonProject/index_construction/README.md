## Практичне 4.

### Завдання:

- Побудувати суфіксне дерево термінів словника
- Побудувати перестановочний індекс для словника
- Побудувати 3-грамний індекс для словника
- реалізувати підтримку запитів з джокерами

### input files
- ./input
### output files
- ./output

## Query examples


> ap*ha

## Results:

```sh

{'$al': ['alpha'], 'lph': ['alpha'], 'pha': ['alpha'], 'ha$': ['beha', 'alpha'], '$be': ['bebebe', 'beta', 'beha'], 'eta': ['beta'], 'ta$': ['getta', 'beta'], '$ga': ['gamma'], 'amm': ['gamma'], 'mma': ['gamma'], 'ma$': ['gamma'], '$Al': ['Alla'], 'lla': ['Alla'], 'la$': ['Alla', 'lalala'], 'eha': ['beha'], '$ge': ['getta'], 'ett': ['getta'], 'tta': ['getta'], '$la': ['lalala'], 'ala': ['lalala'], 'lal': ['lalala'], 'ebe': ['bebebe'], 'beb': ['bebebe'], 'be$': ['bebebe'], '$om': ['omega'], 'meg': ['omega'], 'ega': ['omega'], 'ga$': ['omega']}
{'alpha': ['alpha$', '$alpha', 'a$alph', 'ha$alp', 'pha$al', 'lpha$a'], 'beta': ['beta$', '$beta', 'a$bet', 'ta$be', 'eta$b'], 'gamma': ['gamma$', '$gamma', 'a$gamm', 'ma$gam', 'mma$ga', 'amma$g'], 'Alla': ['Alla$', '$Alla', 'a$All', 'la$Al', 'lla$A'], 'beha': ['beha$', '$beha', 'a$beh', 'ha$be', 'eha$b'], 'getta': ['getta$', '$getta', 'a$gett', 'ta$get', 'tta$ge', 'etta$g'], 'lalala': ['lalala$', '$lalala', 'a$lalal', 'la$lala', 'ala$lal', 'lala$la', 'alala$l'], 'bebebe': ['bebebe$', '$bebebe', 'e$bebeb', 'be$bebe', 'ebe$beb', 'bebe$be', 'ebebe$b'], 'omega': ['omega$', '$omega', 'a$omeg', 'ga$ome', 'ega$om', 'mega$o']}
>>>>>>> Make a query: al*ha
start_word  al
end_word  ha
List with start1: ['alpha']
List with end2: ['beha', 'alpha']
List with start: ['alpha']
List with end: ['beha', 'alpha']
List 1: ['alpha']
List 2: ['beha', 'alpha']
Found:  ['alpha']

```

```sh

>>> Make a query: be
word  be
middle_word: be
Found: ['beta', 'bebebe', 'beha']
Found: ['bebebe']
List 1: ['beta', 'bebebe', 'beha']
List 2: ['bebebe']
Found:  ['bebebe']

```

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1644961389/Screenshot_2022-02-15_at_23.33.32_unzk5j.png)

## To run:
 
- python main.py

###Requirements
`
Python 2.7.16
or 3
`

### Repo:
https://bitbucket.org/svetlana_khmel/naukma_python_experiments/src/master/pythonProject/index_construction/


### Tutorial:
https://docs.python.org/3/tutorial/modules.html