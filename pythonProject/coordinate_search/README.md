## Методи інформаційного пошуку
### Практичне зайняття 3. Двословний індекс і координатний інвертований індекс

#### The tack:

Побудувати двословний індекс і координатний інвертований індекс по колекції документів.

Реалізувати фразовий пошук та пошук з урахуванням відстані для кожного з них.

### screenshots
- ./screenshots
### input files
- ./txt
### output files
- ./output

## Query examples
!! Працює пошук тільки по 2-м словам 

> gentlewomen which

## Results:

```sh
> Make a query: jewel and misconceived
>>> intersection:  [9, 10, 3]
Document:  9
Found on positions: []
Document:  10
Found on positions: []
Document:  3
Found on positions: [27888]

```

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1643904510/Screenshot_2022-02-03_at_18.08.02_prtiyk.png)


## To run:
 
- python main.py

###Requirements
`
Python 2.7.16
or 3

`

### Repo:
https://bitbucket.org/svetlana_khmel/naukma_python_experiments/src/master/pythonProject/coordinate_search/