# coding=utf-8
import os
import re
import sys
import json

#/\b(\w+\D\S)\b/g
inputDirectory = "txt/"
# inputDirectory = "tmp/"
list_of_files = []
row_dictionaries = {}
pair_row_dictionaries = {}
incidentMatrix = {}
invertIndex = {}

coordinateIndex = {}
search_result = []

def getFileSize(name):
    size = os.path.getsize('./output/'+name)
    print('Size of '+ name +' file is', size, 'bytes')

def writeToFile(name, extention, content):
    if extention == 'txt':
        with open('output/'+ name, 'w') as f:
            f.write(str(content))
            # getFileSize(name)
    if extention == 'json':
        json_object = json.dumps(content, indent=4)
        with open('output/'+name, 'w') as f:
            json.dump(content, f)
            # getFileSize(name)

def getSize():
    print('incidentMatrix Size: ', sys.getsizeof(incidentMatrix))
    print('invertIndex Size: ', sys.getsizeof(invertIndex))

def foundInDocuments(documents_num, type):
    print('Result found in documents: ')

    if type == 'incident':
        for i in range(len(documents_num)):
            if documents_num[i] == 1:
                print(list_of_files[i])

    if type == 'invert':
        for i in range(len(documents_num) - 1):
            print(list_of_files[documents_num[i] - 1])

def getList(dict):
    list = []

    if dict == None:
        return
    for key in dict.keys():
        list.append(key)

    return list

def Search(list_1, list_2, search_step=1):
    step = search_step
    index_1 = 0
    index_2 = 0
    while index_1 < len(list_1):
        if list_1[index_1] + step == list_2[index_2]:
            search_result.append(list_1[index_1])
            index_1 += 1
            index_2 += 1
            continue

        if list_1[index_1] + step < list_2[index_2]:
            index_1 += 1
            continue

        if list_1[index_1] + step > list_2[index_2]:
            index_2 += 1
            continue

    print('Found on positions:', search_result)

def getInversionAND(list1, list2):
    result = []
    if list1 and list2:
        result = list(set(list1) & set(list2))
        print('**** >> Inversion AND result: ', result)

    return result

preliminary_results = []
preliminary_positions_results = []

def invertionSearch(phrase):

    for word in phrase:
        row_dictionaries.get(word)

        keys = getList(row_dictionaries.get(word))

        if not keys:
            print('Not found')
            return

        if len(keys) > 0:
            preliminary_results.append([keys])

    print('>>> preliminary_results: ', preliminary_results)

    for i in range(len(phrase)):
        word_1 = preliminary_results[i]

        if not preliminary_results[i+1]:
            return
        word_2 = preliminary_results[i+1]

        intersection = getInversionAND(word_1[0], word_2[0]) # We will search in these dictionaries
        print('>>> intersection: ', intersection)

        for el in intersection:
            for word in phrase:
                preliminary_positions_results.append(row_dictionaries[word].get(el))

        coordinate_search(intersection, preliminary_positions_results)

def coordinate_search(intersection, preliminary_positions_results):
    for ind, item in enumerate(preliminary_positions_results):
        if intersection[ind]:
            print('Document: ', intersection[ind])
        if len(preliminary_positions_results[ind + 1]) > 0:
            positions_1 = preliminary_positions_results[ind]
            positions_2 = preliminary_positions_results[ind + 1]
            Search(positions_1, positions_2)

def coordinateIndexSearch():
    query = input('Make a query: ')
    phrase = query.split()

    invertionSearch(phrase)

def createCoordinateIndexDocument(processedText, index):
    for x in range(len(processedText)):
        documentNumber = index + 1
        wordPosition = x
        currentWord = processedText[x]

        if not currentWord in row_dictionaries:
            row_dictionaries[currentWord] = {}

        if not row_dictionaries[currentWord].get(documentNumber):
            row_dictionaries[currentWord][documentNumber] = []

        row_dictionaries[currentWord][documentNumber].append(wordPosition)

def createTwoWordIndexDocument(processedText, index):
    for x in range(len(processedText)):
        documentNumber = index + 1
        wordPosition = x
        word2 = ''

        if x < len(processedText) - 1:
            word2 = processedText[x + 1]

        pair = processedText[x] + ' ' + word2
        currentWord = pair

        if not currentWord in pair_row_dictionaries:
            pair_row_dictionaries[currentWord] = {}

        if not pair_row_dictionaries[currentWord].get(documentNumber):
            pair_row_dictionaries[currentWord][documentNumber] = []

        pair_row_dictionaries[currentWord][documentNumber].append(wordPosition)

def processFile(text, index):
    processedText = re.sub(r'[^\w\s]', '', text)

    createCoordinateIndexDocument(processedText.split(), index)
    createTwoWordIndexDocument(processedText.split(), index)


def readDirectory():
    for filename in os.listdir(inputDirectory):

        index = len(list_of_files)
        subarray = [index, filename]
        list_of_files.append(subarray)

        with open(os.path.join(inputDirectory, filename), 'r') as f:
            text = f.read()
            processFile(text, index)

    print('Coordinate Index >>> ', row_dictionaries)
    print('2 word index >>> ', pair_row_dictionaries)

    # getSize()
    writeToFile('Coordinate_Index.txt', 'txt', row_dictionaries)
    writeToFile('Two_word_index.txt', 'txt', pair_row_dictionaries)

    writeToFile('Coordinate_Index.json', 'json', row_dictionaries)
    writeToFile('Two_word_index.json', 'json', pair_row_dictionaries)

readDirectory()
coordinateIndexSearch()
print('Pair index dictionary length:', len(pair_row_dictionaries))
print('Coordinaate index dictionary length:', len(row_dictionaries))
