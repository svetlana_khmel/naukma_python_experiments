# This is a sample Python script.
# T06_11_v1
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import random2 as random

def ChekForQueens(num):
    NumConflicts = 0
    x = Genlist[1][num]
    y = Genlist[2][num]
    for i in range(n):
        if i != num:
            if abs(x - Genlist[1][i]) == abs(y - Genlist[2][i]):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]
            elif (x == Genlist[1][i]):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]
            elif (y == Genlist[2][i]):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]

            horsePositions = [[-2, -1],
                              [-2, +1],
                              [-1, +2],
                              [+1, +2],
                              [+2, +1],
                              [+2, -1],
                              [+1, -2],
                              [-1, -2]]

            for t in range(8):
                if (x + horsePositions[t][0] == Genlist[1][i]) and (
                        y + horsePositions[t][1] == Genlist[2][i]) and i >= NumQeens:
                    ConflictWeight[num][i] += 1
                    NumConflicts += ConflictWeight[num][i]
    return NumConflicts


def ChekForHorses(num):
    NumConflicts = 0
    x = Genlist[1][num]
    y = Genlist[2][num]
    for i in range(n):
        if i != num:
            if abs(x - Genlist[1][i]) == abs(y - Genlist[2][i]) and (i < NumQeens):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]
            elif (x == Genlist[1][i]) and (i < NumQeens):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]
            elif (y == Genlist[2][i]) and (i < NumQeens):
                ConflictWeight[num][i] += 1
                NumConflicts += ConflictWeight[num][i]

            horsePositions = [[-2, -1],
                              [-2, +1],
                              [-1, +2],
                              [+1, +2],
                              [+2, +1],
                              [+2, -1],
                              [+1, -2],
                              [-1, -2]]

            for t in range(8):
                if (x + horsePositions[t][0] == Genlist[1][i]) and (
                        y + horsePositions[t][1] == Genlist[2][i]):
                    ConflictWeight[num][i] += 1
                    NumConflicts += ConflictWeight[num][i]

    return NumConflicts


def ChekPoss(num):
    if num < NumQeens:
        return ChekForQueens(num)
    else:
        return ChekForHorses(num)


def PsevdoChekQeens(num, x, y):
    NumConflicts = 0
    for i in range(n):
        if i != num:
            if abs(x - Genlist[1][i]) == abs(y - Genlist[2][i]):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]
            elif (x == Genlist[1][i]):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]
            elif (y == Genlist[2][i]):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]

            horsePositions = [[-2, -1],
                              [-2, +1],
                              [-1, +2],
                              [+1, +2],
                              [+2, +1],
                              [+2, -1],
                              [+1, -2],
                              [-1, -2]]

            for t in range(8):
                if (x + horsePositions[t][0] == Genlist[1][i]) and (
                        y + horsePositions[t][1] == Genlist[2][i]) and i >= NumQeens:
                    NumConflicts += 1
                    NumConflicts += ConflictWeight[num][i]
    return NumConflicts


def PsevdoChekHorses(num, x, y):
    NumConflicts = 0
    for i in range(n):
        if i != num:
            if abs(x - Genlist[1][i]) == abs(y - Genlist[2][i]) and (i < NumQeens):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]
            elif (x == Genlist[1][i]) and (i < NumQeens):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]
            elif (y == Genlist[2][i]) and (i < NumQeens):
                NumConflicts += 1
                NumConflicts += ConflictWeight[num][i]

            horsePositions = [[-2, -1],
                              [-2, +1],
                              [-1, +2],
                              [+1, +2],
                              [+2, +1],
                              [+2, -1],
                              [+1, -2],
                              [-1, -2]]

            for t in range(8):
                if (x + horsePositions[t][0] == Genlist[1][i]) and (
                        y + horsePositions[t][1] == Genlist[2][i]):
                    NumConflicts += 1
                    NumConflicts += ConflictWeight[num][i]
    return NumConflicts
    # return NumConflicts


def PsevdoChek(num, x, y):
    if num < NumQeens:
        return PsevdoChekQeens(num, x, y)
    else:
        return PsevdoChekHorses(num, x, y)


def FindBestPoss(num):
    NumberSloved = PsevdoChek(num, Genlist[1][num], Genlist[2][num])
    h = NumberSloved
    XBestPoss = Genlist[1][num]
    YBestPoss = Genlist[2][num]
    for i in range(m):
        for j in range(k):
            if (Board[i][j] == 0):
                # z = PsevdoChek(num, i, j)
                if PsevdoChek(num, i, j) == 0:
                    return ConflictList[num], i, j
                if PsevdoChek(num, i, j) < NumberSloved:
                    XBestPoss = i
                    YBestPoss = j
                    NumberSloved = PsevdoChek(num, i, j)

    return h - NumberSloved, XBestPoss, YBestPoss


# m = int(input("Board size m="))
# k = int(input("Board size k="))
#
# NumQeens = int(input("Queens number "))
# NumHorses = int(input("Horses number "))
m = 10
k = 10
NumQeens = 3

NumHorses = 3

n = NumHorses + NumQeens
Board = []
for i in range(m):  #Creating a boatd , fill with 0
    Board.append([])
    for j in range(k):
        Board[i].append(0)

Genlist = [[], [], []]
for i in range(n):
    if i <= NumQeens - 1:
        Genlist[0].append(1)  # Stvorennya ryadky typiv figur
    else:
        Genlist[0].append(2)
Genlist[1] = random.sample(range(m), n)  # Stvorennya pochatkovih koordinat figur
print('range(k) ', range(k));
print('n ', n);
Genlist[2] = random.choice(range(k))

for i in range(n):
    Board[Genlist[1][i]][Genlist[2][i]] = i + 1  # Zakinchennya stvorennya vipadkovo papovnenoi doshki

for i in range(m):
    print(Board[i])
print('Pause      ')
ConflictWeight = []
for i in range(n):
    ConflictWeight.append([])
    for j in range(n):  # Zapovnennuyu massiv konfliktiv nulyami
        ConflictWeight[i].append(0)

ListBestPoss = []
ConflictList = []
ListBestResault = []

for i in range(n):
    ConflictList.append(ChekPoss(i))
    ListBestPoss.append([])
    ListBestResault.append(0)

for i in range(3):
    print(Genlist[i])

print('Pause  ')
print(ConflictList)

print('Pause  ')
for i in range(n):
    print('The best PsevdoChek)    ', FindBestPoss(i))

    print(PsevdoChek(i, FindBestPoss(i)[1], FindBestPoss(i)[2]))

while max(ConflictList) > 0:
    for i in range(n):
        ListBestPoss[i] = FindBestPoss(i)
        ListBestResault[i] = ListBestPoss[i][0]
    oops = ListBestResault.index(
        max(ListBestResault))  # oops - zhe nomer figuri,  peremizshennya, yakoi virishit' naibilshu kilkist konfliktiv
    x = Genlist[1][oops]
    y = Genlist[2][oops]
    Board[x][y] = 0
    Genlist[1][oops] = ListBestPoss[oops][1]  # Obnovluyu doshkuu ta matrizyu poziziy figur
    Genlist[2][oops] = ListBestPoss[oops][2]
    Board[Genlist[1][oops]][Genlist[2][oops]] = oops + 1

    for i in range(n):
        ConflictList[i] = ChekPoss(
            i)  # Spisok konfliktiv i za odno obnovluyu matrizyu wagiv pid chas roboti functzii  ChekPos

for i in range(m):
    print(Board[i])
print('List of conflicts: ', ConflictList)
# for i in range(n):
#     ListBestPoss[i] = FindBestPoss(i)
#     ListBestResault.append(ListBestPoss[i][0])
#
# oops= ListBestResault.index(max(ListBestResault))
#
# print(oops)
