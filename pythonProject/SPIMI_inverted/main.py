# SPIMI-invert(token_stream)
#
# output_file = NewFile()
# Dictionary = NewHash()
# 	while(free memory available)
# 	do token -> next(token_stream)
# 	if term(token) !E dictionary
# 		then posting_list = AddToDictionary(dictionary, term(token))
# 		else posting_list = GetPostingList(dictionary, term(token)) 	if full(posting_list)
# 		then posting_list = DoublePostingList(dictionary, term(token))
#
# 	addToPostingList(postings_list, docID(token))
# 	sorted_terms -> sortedTerms(dictionary)
# 	writeBlockToDisc(sorted_terms, dictionary, output_file)
# 	return output_file

import os
import re
import sys
import uuid
import shutil

import write_to_file

list_of_files = []
output = {}

INPUT_DIRECTORY = "./input/"
PARTITIONS_DIRECTORY = 'partitions/'
OUTPUT_DIRECTORY = "./output/"
TERMS_DIRECTORY = "./terms/"
MEMORY_SIZE_LIMIT = 500 # bytes

partitionsPath = os.path.join('./', PARTITIONS_DIRECTORY)
outputPath = os.path.join('./', OUTPUT_DIRECTORY)
outputFile = outputPath+'output.txt'
outputFile = outputPath+'output.txt'

list_of_parts = []

def readTerms():
    for filename in os.listdir(TERMS_DIRECTORY):
        print('filename ::: ', filename)
        with open(os.path.join(TERMS_DIRECTORY, filename), 'r') as f:
            term = filename[:-4]
            text = f.read()
            file = open('./output/SPIMI_indicies' + '.txt', 'w') # here can be a problem with a flag 'w', so it writes several times
            file.write('%s:%s\n' % (term, text[:-1].split(',')))

            file.close()

def writeTerms():
    with open(outputFile, 'r+') as f:
        terms = f.readlines()
        shutil.rmtree(TERMS_DIRECTORY)
        os.mkdir(TERMS_DIRECTORY)

        for term in terms:
            term_out = term.split(":")[0]
            term_part_positions = eval(term.split(":")[1])[0]

            file = open(TERMS_DIRECTORY + term_out+'.txt', 'a+')
            file.write('%s,' % term_part_positions)

            file.close()

def mergePartitions():
    for filename in os.listdir(PARTITIONS_DIRECTORY):
        index = len(list_of_parts)
        list_of_parts.append(filename)

        with open(os.path.join(PARTITIONS_DIRECTORY, filename), 'r') as file_partition:
            file_partition_text = file_partition.readlines()

            for line_part in file_partition_text:
                with open(outputFile, 'r+') as f:
                    if line_part in f.read():
                       print('')
                    else:
                        f.write(line_part)

def spimiInvert(processedText, index):
    dictionary = {}

    for term in processedText:
        if sys.getsizeof(dictionary) < MEMORY_SIZE_LIMIT:
            if not dictionary.get(term):
                dictionary[term] = []

            dictionary[term].append(index)
            dictionary[term] = list(set(dictionary[term]))
        else:
            write_to_file.writeToFile(str(uuid.uuid4())+'.txt', 'txt', dictionary, PARTITIONS_DIRECTORY)
            dictionary = {}

    mergePartitions()
    writeTerms()
    readTerms()


def processFile(text, index):
    processedText = re.sub(r'[^\w\s]', '', text)
    # Будуємо SPIMI
    spimiInvert(processedText.split(), index)

def readDirectory():
    shutil.rmtree(partitionsPath)
    os.mkdir(partitionsPath)

    shutil.rmtree(outputPath)
    os.mkdir(outputPath)
    open(outputFile, mode='w').close()

    for filename in os.listdir(INPUT_DIRECTORY):
        index = len(list_of_files)
        subarray = [index, filename]
        list_of_files.append(subarray)

        with open(os.path.join(INPUT_DIRECTORY, filename), 'r') as f:
            text = f.read()
            processFile(text, index)

readDirectory()
