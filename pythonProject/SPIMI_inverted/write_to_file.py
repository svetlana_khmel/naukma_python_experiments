import json

def writeToFile(name, extention, content, folder='output/'):
    if extention == 'txt':
        file = open(folder + name, 'w')
        for key, value in content.items():
            file.write('%s:%s\n' % (key, value))
        file.close()
    if extention == 'json':
        json_object = json.dumps(content, indent=4)
        with open(folder + name, 'w') as f:
            json.dump(content, f)
