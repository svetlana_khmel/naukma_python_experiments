## Практичне 5.

### Завдання:

Обрати один з методів побудови індексу:

- BSBI
- SPIMI
- MapReduce
- Динамічна побудова
- 
Та побудувати індекс великої колекції.

### input files
- ./input
### output files
- ./output
### SPIMI indexes
- ./output/SPIMI_indicies.txt





## To run:
 
- python main.py

###Requirements
`
Python 2.7.16
or 3
`

pip install pathlib2
pip install python-util 
