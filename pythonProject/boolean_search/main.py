# coding=utf-8
import os
import re
import sys
import json

#/\b(\w+\D\S)\b/g

list_of_files = []
row_dictionaries = {}
incidentMatrix = {}
invertIndex = {}

def bullAND(arg1, arg2) :
    if arg1 == 1 and arg2 == 1:
        return 1
    if arg1 == 1 and arg2 == 0:
        return 0
    if arg1 == 0 and arg2 == 1:
        return 0
    if arg1 == 0 and arg2 == 0:
        return 0

def bullOR(arg1, arg2) :
    if arg1 == 1 and arg2 == 1:
        return 1
    if arg1 == 1 and arg2 == 0:
        return 1
    if arg1 == 0 and arg2 == 1:
        return 1
    if arg1 == 0 and arg2 == 0:
        return 0

def bullNOT(arg1) :
    # not_arg = 1 - arg2

    if arg1 == 0:
        return 1
    if arg1 == 1:
        return 0

def getFileSize(name):
    size = os.path.getsize('./output/'+name)
    print('Size of '+ name +' file is', size, 'bytes')

def writeToFile(name, extention, content):
    if extention == 'txt':
        with open('output/'+ name, 'w') as f:
            f.write(str(content))
            getFileSize(name)
    if extention == 'json':
        json_object = json.dumps(content, indent=4)
        with open('output/'+name, 'w') as f:
            json.dump(content, f)
            getFileSize(name)

def getSize():
    print('incidentMatrix Size: ', sys.getsizeof(incidentMatrix))
    print('invertIndex Size: ', sys.getsizeof(invertIndex))

def getInversionAND(word1, word2):
    word1_invert_matrix = invertIndex[word1]
    word2_invert_matrix = invertIndex[word2]

    result = list(set(word1_invert_matrix) & set(word2_invert_matrix))
    print('**** >> Inversion AND result: ', result)

    foundInDocuments(result, 'invert')

    # next_matrix_a = true
    # next_matrix_b = true
    #
    # indexA = 0
    # indexB = 0

    # while (next_matrix_a and next_matrix_b):
    #     print('next_matrix_a..... ', next_matrix_a)
    #     print('next_matrix_b..... ', next_matrix_b)
    #
    #     if word1_invert_matrix[index] == word2_invert_matrix[index]:
    #         result.append(word1_invert_matrix[index])
    #         indexA += 1
    #         indexB += 1
    #     elif word1_invert_matrix[index] < word2_invert_matrix[index]:
    #         indexA += 1
    #
    #         if not word1_invert_matrix[index]:
    #             next_matrix_a = false
    #
    #     elif word1_invert_matrix[index] > word2_invert_matrix[index]:
    #         indexB += 1
    #
    #         if not word2_invert_matrix[index]:
    #             next_matrix_b = false

def getInversionOR(word1, word2):
    word1_invert_matrix = invertIndex[word1]
    word2_invert_matrix = invertIndex[word2]

    result = list(set(word1_invert_matrix))+list(set(word2_invert_matrix))
    print('**** >> Inversion AND result: ', result)

    foundInDocuments(result, 'invert')

def getInversionNOT(word):
    word_invert_matrix = invertIndex[word]
    document_nums = []

    for n in range(len(list_of_files)):
        document_nums.append(n)

    result = list(set(document_nums).symmetric_difference(word_invert_matrix))
    print('**** >> Inversion OR result: ', result)

    foundInDocuments(result, 'invert')

def foundInDocuments(documents_num, type):
    print('Result found in documents: ')

    if type == 'incident':
        for i in range(len(documents_num)):
            if documents_num[i] == 1:
                print(list_of_files[i])

    if type == 'invert':
        for i in range(len(documents_num) - 1):
            print(list_of_files[documents_num[i] - 1])


def invertionSearch(word1, word2, conditionWord):
    word1_invert_matrix = invertIndex[word1]
    print('Word 1 invert matrix:', word1_invert_matrix)

    if not conditionWord == 'not':
        word2_invert_matrix = invertIndex[word2]

        print('Word 2 invert matrix:', word2_invert_matrix)

    if conditionWord == 'not':
        getInversionNOT(word1)

    elif conditionWord == 'and':
        getInversionAND(word1, word2)

    elif conditionWord == 'or':
        getInversionOR(word1, word2)


def binarySearch():
    query = raw_input('Make a query: ')
    word1 = query.split()[0]
    conditionWord = query.split()[1].lower()

    print(word1, incidentMatrix[word1])
    print(conditionWord)

    if conditionWord == 'not':
        word2 = None
    else:
        word2 = query.split()[2]
        print(word2, incidentMatrix[word2])

    result = []

    for i in range(len(incidentMatrix.get(word1))):
        matrix1 = incidentMatrix[word1][i]

        if conditionWord == 'not':
            result.append(bullNOT(matrix1))

        elif conditionWord == 'and':
            matrix2 = incidentMatrix[word2][i]
            result.append(bullAND(matrix1, matrix2))

        elif conditionWord == 'or':
            matrix2 = incidentMatrix[word2][i]
            result.append(bullOR(matrix1, matrix2))
            # print('OR result: ', result)


    print('Result: ', result)

    foundInDocuments(result, 'incident')
    invertionSearch(word1, word2, conditionWord)

def buildInvertIndex():
    for element in incidentMatrix:
        incident_places = incidentMatrix[element]
        invertIndex[element] = []

        for i in range(len(incident_places)):
          if incident_places[i] == 1:
              invertIndex[element].append(i + 1)
              # print('.. ', invertIndex[element])


   # if i == len(incident_places):
        #     print(1)
        #     binarySearch()

def buildIncidentMatrix():
    filesNum = len(os.listdir('./txt/'))

    for index, key in enumerate(row_dictionaries):
       array = row_dictionaries[key]

       for element in array:
           if element not in incidentMatrix:
               incidentMatrix[element] = [0 for x in range(filesNum)]
               incidentMatrix[element][index] = 1
           else:
               incidentMatrix[element][index] = 1

    buildInvertIndex()

def buildRawDictionary(array, index):
    row_dictionaries[index] = {}

    for element in array:
      if element not in row_dictionaries[index]:
         row_dictionaries[index][element] = 1

      if element in row_dictionaries[index]:
         row_dictionaries[index][element] += 1

    buildIncidentMatrix()


def processFile(text, index):
    processed = re.sub(r'[^\w\s]', '', text)

    buildRawDictionary(processed.split(), index)

def readDirectory():
    for filename in os.listdir("txt/"):

        index = len(list_of_files)
        subarray = [index, filename]
        list_of_files.append(subarray)

        with open(os.path.join("txt/", filename), 'r') as f:
            text = f.read()
            processFile(text, index)

readDirectory()
binarySearch()


# print('incidentMatrix : ', incidentMatrix)
# print('invertIndex : ', invertIndex)
getSize()
writeToFile('incidentMatrix.txt', 'txt', incidentMatrix)
writeToFile('invertIndex.txt', 'txt', invertIndex)

writeToFile('incidentMatrix.json', 'json', incidentMatrix)
writeToFile('invertIndex.json', 'json', invertIndex)



    #if __name__ == '__main__':


# encodings = ['utf-8', 'windows-1250', 'windows-1252'] # add more
# for e in encodings:
#     try:
#         fh = codecs.open('file.txt', 'r', encoding=e)
#         fh.readlines()
#         fh.seek(0)
#     except UnicodeDecodeError:
#         print('got unicode error with %s , trying different encoding' % e)
#     else:
#         print('opening the file with encoding:  %s ' % e)
#         break

###### Boolian search ###
# Example:

# Find:
# jewel and misconceived:
# 'jewel':        [0, 1, 0, 1, 1, 1, 0, 1, 1, 1],
# 'misconceived': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],

# RESULS will BE: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
# Result found in documents:
# [1, 'henry-vi-part-1_TXT_FolgerShakespeare.txt']

# ('Word 1 invert matrix:', [2, 4, 5, 6, 8, 9, 10])
# ('Word 2 invert matrix:', [2])
# ('**** >> Inversion AND result: ', [2])


# 'runaways':     [0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
# 'northerly':    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],



