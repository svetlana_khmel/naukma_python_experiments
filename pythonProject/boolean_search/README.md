## SEARCH SYSTEMS
### Boolean search

#### The tack:

По заданій колекції (10 документів по 150К) документів побудувати:
матрицю інцидентності "термін-документ"
інвертований індекс
Обгрунтуйте обрані структури збереження даних в розрізі їх ефективності при збільшенні об'ємів даних.

Порівняти розмір структур, що вийшли.

Зробити булевий пошук по цим структурам (по обом).

Оператори: AND, OR, NOT. Формат в запиті на власний розсуд

### input files
- ./txt
### output files
- ./output

## Query examples

> Make a query: jewel and misconceived
> Make a query: jewel or misconceived
> Make a query: jewel not

## Results:

```sh
> Make a query: jewel and misconceived
> ('jewel', [0, 1, 0, 1, 1, 1, 0, 1, 1, 1])
and
('misconceived', [0, 1, 0, 0, 0, 0, 0, 0, 0, 0])
('Result: ', [0, 1, 0, 0, 0, 0, 0, 0, 0, 0])
Result found in documents: 
[1, 'henry-vi-part-1_TXT_FolgerShakespeare.txt']
('Word 1 invert matrix:', [2, 4, 5, 6, 8, 9, 10])
('Word 2 invert matrix:', [2])
('**** >> Inversion AND result: ', [2])
Result found in documents: 
[1, 'henry-vi-part-1_TXT_FolgerShakespeare.txt']
('incidentMatrix Size: ', 786712)
('invertIndex Size: ', 786712)
('Size of incidentMatrix.txt file is', 811008, 'bytes')
('Size of invertIndex.txt file is', 389120, 'bytes')
('Size of incidentMatrix.json file is', 811008, 'bytes')
('Size of invertIndex.json file is', 389120, 'bytes')

```

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1643231340/Screenshot_2022-01-26_at_23.07.55_efkma9.png)

## To run:
 
- python main.py
- input words from incidentMatrix to get search result

###Requirements
`
Python 2.7.16
`

### Repo:
https://bitbucket.org/svetlana_khmel/naukma_python_experiments/src/master/pythonProject/boolean_search/